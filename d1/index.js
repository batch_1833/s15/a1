// // console.log("Hellog World!");

// // [SECTION] Assignment Operator

// // Basic Assignment Operator (=)
// // it adds the value of the right operand to a variable an

// let = assignmentNumber = 8;

// // Addition Assignment Operator
// // 8				8+2
// // assignmentNumber = assignmentNumber +2;
// // console.log("Result of the Addition Assignment Operator: " + assignmentNumber);

// // Shorthand method
// assignmentNumber += 2;
// console.log("Result of the Addition Assignment Operator: " + assignmentNumber);

// // Substraction(-=)/Multiplication(*=)/Division Assignment Operator(/=)

// // Subtraction
// assignmentNumber -= 2;
// console.log("Result of the Addition Assignment Operator: " + assignmentNumber);

// // Multiplication
// assignmentNumber *= 2;
// console.log("Result of the Addition Assignment Operator: " + assignmentNumber);

// // Division
// assignmentNumber /= 2;
// console.log("Result of the Addition Assignment Operator: " + assignmentNumber);

// // [SECTION] Arithmetic Operators

// let x = 21;
// let y = 9;

// // Addition
// let sum = x +  y;
// console.log("Result of Addition Operator: "+sum);

// // Subtraction
// let difference = x - y;
// console.log("Result of Subtraction Operator: "+difference);

// // Multiplication
// let product = x * y;
// console.log("Result of Multiplication Operator: "+product);

// // Division
// let qoutient = x / y;
// console.log("Result of Division Operator: "+qoutient);

// // Remainder
// let modulo = x % y;
// console.log("Result of modulo Operator: " +modulo);

// // Multiple Operators and Perenthesis

// // PEMDAS - Parenthesis, exponent, multiplication, division, addition, subtraction rule

// let mdas = 1+2-3*4/5
// console.log("Result of mdas Operatorion: "+mdas.toFixed(2))

// let pemdas = 1+(2-3) * (4/5);
// console.log("Result of pemdas Operatorion: "+pemdas.toFixed(2))

// // Increment and Decrement 
// // This operators add or substract values by 1 and reassign the value of the variable where the increment/decrement was applied to.

// let z=1;
// console.log("Result of z before increment/decrement "+z);

// // Preincrement
// 				// 2
// let increment = ++z;
// console.log("Result of z before pre-increment: "+increment);
// console.log("Result of z before pre-increment: "+z);

// // Post Increment
// 			// 1
//  increment = z++;
// console.log("Result of z before post increment: "+increment);
// console.log("Result of z before post increment: "+z);

// // Predecrement
// 				// 2
// let decrement = --z;
// console.log("Result of z before pre-increment: "+decrement);
// console.log("Result of z before pre-increment: "+z);

// // Post decrement
// 			// 1
//  decrement = z--;
// console.log("Result of z before post increment: "+decrement);
// console.log("Result of z before post increment: "+z);

// // [SECTION] Type Coercion
// // automatic or implicit conversion of values from one data type to another.

// let numA = '10';
// let numB = 12;

// let coercion = numA + numB;
// console.log(coercion);
// //typeof it is used to show the data type of a specific variable. 
// console.log(typeof coercion);

// // example of nonCoercion
// let numC = 16;
// let numD = 14;

// let nonCoercion = numC + numD;
// console.log(nonCoercion);
// console.log(typeof nonCoercion);

// // boolean "true" is also associated with the value of 1.  
// let numE = true + 1;
// console.log(numE);
// console.log(typeof numE);

// let numF = false + 1;
// console.log(numF);

// // [SECTION] Comparison Operators

// // (=) used for assignment
// let juan = 'juan';

// // Equality Operator (==)
// // Checks wether the operands are equal/have the same content.
// // Attempts to CONVERT and COMPARE operands of different data type.
// // Returns a Boolean

// console.log(1==1);
// console.log(1==2);
// console.log(1 == '1');
// console.log(0 == false);
// console.log(1==true);
// // comparison with string is case sensitive.
// console.log('juan' == 'juan');
// // compares a string with the variable "juan" declared above.
// console.log(juan=='juan');

// // Inequality operator (!=);
// // checks whether the operands are note equal/have different content.
// console.log(1 !=1);
// console.log(1 !=2);
// console.log(1 !='1');
// console.log(0 != false);
// console.log(1!=true);

// // Strict Equality Operator (===)
// // Checks whether the operands are equal/have the  same content.
// // also compares the data type of 2 values.
// console.log(1===1);
// console.log(1===2);
// console.log(1==='1');
// console.log(0=== false);
// console.log(1===true);
// // compares the value of the variable to the other operand.
// console.log(juan === 'juan');

// // Strict Inequality Operator(!==)
// // Checks wether the operands are not equal/have different content.
// // also Compares the data type of 2 values.
// console.log(1!==1);
// console.log(1!==2);
// console.log(1!=='1');
// console.log(0!==false);
// console.log(1!==true);

// //  < >  <=  >=
// console.log("less than and greater than");
// console.log(4<6);
// console.log(2>8);
// console.log(9>9);
// // greater than or equal
// console.log(9>=9);
// console.log(10<=15);

// // [SECTION] Logical Operators

// let isLegalAge =true;
// let isRegisterd = false;

// // Logical AND Operator (& - Double ampersand);
// // Returns true if all operands are true
// let AllRequirementsMet = isLegalAge && isRegisterd;
// console.log("Result of Logical AND Operator: " + AllRequirementsMet);

// console.log(true && true); //true
// console.log(true && false); //false
// console.log(false && true); //false
// console.log(false && false); //false

// // Logical OR Operator (|| - Double Pipe)
// // Returns true if one operands are true
// let someRequirementsMet = isLegalAge || isRegisterd;
// console.log("Result of Logical OR Operator: " + someRequirementsMet);

// // OR operator
// console.log(true || true); //true
// console.log(true || false); //false
// console.log(false || true); //false
// console.log(false || false); //false

// // Logical NOT operator (! - exclamation point)
// // Returns Opposite value
// let someRequirementsNotMe = !isRegisterd;
// console.log("Result of Logical NOT operator: "+ someRequirementsNotMe);
// // NOT operator
// console.log(!true);
// console.log(!false);

// // Multiple Logical Operators
// let isQualified = isLegalAge && isRegisterd || isLegalAge;

// /*
// 	Highest priority NOT
// 	Lowest priority OR
// 	=true && false // false
// 	=false || true //true


// */
// console.log("Result of isQualified: "+ isQualified);

// isQualified = !(isLegalAge && isRegisterd) && isRegisterd;

// // = true && false // false
// // =!false // true
// // =true && false
// // = false

// console.log("Result of isQualified: "+ isQualified);

// [SECTION] if, else if, and else Statement.

/*
	Syntax:
	if(condition){
		//code block

	}

*/

let numG = 1;
// if Statement
//  Executes a statement if a specified contion is true
if(numG < 0){
	console.log("Hello World");
}
// else if CLause
// Executes a statement if previous conditions are false and if the specified condtion is true.
else if(numG == 1){
	console.log("My");
}
// else Statement
// Executes a statement if all other conditions are false
else{
	console.log("World");
}

// if , else if , and else Statement with function
/*

	Scenario: We want to determine intensity of a typhone based on its wind speed.
		Not a Typoon - Wind speed is less than 30. (windSpeed < 30) 
		Tropical Depression - Wind speed is greater than or equal to 61.(windSpeed <= 61)
		Tropical Storm - Wind speed is between 62 to 88. (windSpeed >= 62 && <=88)
		Severe Tropical Storm - Wind speed is between 89 to 117. (windSpeed >= 89 && <=117)
		Typoon - Wind speed is greater than or equal to 118. (windSpeed >= 118)

*/

let message = "No message";
console.log(message)

function determineTyphoonIntensity(windSpeed){
		if(windSpeed <30){
			return "Not a Typoon";
		}
		else if (windSpeed <= 61){
			return "Tropical Depression Detected";
		}
		else if (windSpeed >= 62 && windSpeed<=88){
			return "Tropical Storm Detected";
		}
		else if (windSpeed >= 89 && windSpeed<=117){
			return "Severe Tropical Storm";
		}
		else{
			return "Typoon Detected";
		}
}

//  Returns the string to the variable message that invoked it.
message = determineTyphoonIntensity(110);
console.log(message);

// [SECTION] Truthy and Falsy
// a "truthy " values is a values is considered true when encountered in a Boolean context.

// Example of Truthy - true|1|[]
// let result = [];
// if (result){
// 	console.log("Truthy");
// }

// Examples of Falsy - false | 0 | undefined |null
// let result = null;

// if (result){
// 	console.log("Falsy");
// }

// [SECTION] Ternary Operator
// Single statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log(ternaryResult);

// prompt()
// create a pop-ip message in the browser that can be used to gather user input.

// Create a conditional statement that will check the age of a person and will determine if he/she is qualified to vote. If the person's age is 18 years old and above print the message "You're qualified to vote!" else print "Sorry, You're too young to vote."

// Sample Output:
// //prompt
// Enter your age = 17;

// //Browser Console:
// Sorry, You're too young to vote.


// parseInt()
// Converts a string variable to integer/number.

function isLegalAge(){
	return "You're qualified to vote.";
}

function isUnderAge(){
	return "Sorry, You're too young to vote.";
}


let age = parseInt(prompt("What is your age?"));

//if (age>=18){
	//it displays an alert box with a message and an OK button.
//	alert(isLegalAge());
//}
//else {
//	alert(isUnderAge())
//}

//let isVoter = (age >=18)? isLegalAge(): isUnderAge();
//alert("Result of Ternary Operator "+isVoter);

// [SECTION] Swtich Case
// can be used as an alternative to an "if, else if, and else" statement where data to be used in the condition is of an expected output.

/*

	Syntax:
		switch(expression){
			case value:
				statement;
				break;

			default:
				statement;
				break;
		}
*/

//let day = prompt("What day of the week is it today?")
//console.log(day);

// toLowerCase() method that will change the input received from the prompt into all lowercase letter.
// let day = promp t("What day of the week is it today?").toLowerCase();
// console.log(day);

// switch(day){
// 	case "monday":
// 		console.log("The color of the day is Red");
// 		break;
// 	case "tuesday":
// 		console.log("The color of the day is Orange");
// 		break;
// 	case "wednesday":
// 		console.log("The color of the day is Yellow");
// 		break;
// 	case "thursday":
// 		console.log("The color of the day is Green");
// 		break;
// 	case "friday":
// 		console.log("The color of the day is Blue");
// 		break;
// 	case "saturday":
// 		console.log("The color of the day is Indigo");
// 		break;
// 	case "sunday":
// 		console.log("The color of the day is Pink");
// 		break;
// 	default:
// 		alert("Please input a valid day!");
// 		break;
// }


// [SECTION] Try-Catch-Finally Statement
// are commonly used for handling errors.

// it Attempts to execute a code
try{
	alerat(determineTyphoonIntesity(56));
}
// catch will handle the error encountered with the try statment
catch (error){
	console.log(typeof error);

	// "error.message" is used to access the information relating to an error object.
	
	// used to write warnings in the console.
	console.warn(error.message);
}
finally{
	// Continue the excution of code regardless of success and failure of code excution in the "try" statement/blocks.
	alert("Intensity updates will show new alert.")
}
